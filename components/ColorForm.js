import React, { Component, PropTypes } from 'react';
import {
  View, Text, StyleSheet,
  TextInput
} from 'react-native';

export default class ColorForm extends Component {
  constructor() {
    super();
    this.state = {
      txtColor: ''
    }
  }
  submit() {
    this.props.onNewColor(this.state.txtColor.toLowerCase())
    this.setState({
      txtColor: ''
    })
  }
  render() {
    const  navigate = this.props.navigation;
    const uri = 'https://www.w3schools.com/html/html_form_input_types.asp';
    return (
      <View style={styles.container}>
        <TextInput placeholder='Enter a color' onChangeText={(txtColor) => this.setState({ txtColor })}
          value={this.state.txtColor}
          style={styles.txtInput}></TextInput>
        <Text onPress={this.submit.bind(this)} style={styles.button}>Add</Text>
        <Text onPress={() => navigate('Web', { uri })} style={styles.button}>Info</Text>
      </View>
    )
  }
}
ColorForm.propTypes = {
  onNewColor: PropTypes.func.isRequired
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'lightgrey',
    justifyContent: 'space-around',
    height: 50,
  },
  txtInput: {
    flex: 1,
    margin: 5,
    padding: 5,
    borderWidth: 2,
    fontSize: 20,
    borderRadius: 5,
    backgroundColor: 'snow'
  },
  button: {
    backgroundColor: 'darkblue',
    padding: 5,
    margin: 5,
    alignItems: 'center',
    color: 'white',
    fontSize: 20
  },
})