import React from 'react';
import {
  Text, View, StyleSheet
}
  from 'react-native';
import ColorTools from 'color';
const ColorDetail = ({ navigation }) => {
  const color = ColorTools(navigation.state.params.color);
  return (
    <View style={[styles.container, { backgroundColor: color }]}>
      <Text style={[styles.text,{color: color.negate()}]}>TODO: Display Color detail</Text>
    </View>
  )
}
ColorDetail.navigationOptions = ({ navigation }) => ({
  title: navigation.state.params.color
})
export default ColorDetail;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 20,
    margin: 10
  }
})
