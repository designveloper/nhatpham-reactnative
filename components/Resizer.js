import React from 'react';
import { func, string, number } from 'prop-types';
import {
    View, StyleSheet, PanResponder
} from 'react-native';
import styled from 'styled-components/native';
import ResizeHelper from './ResizeHelper';


// Size of circle
const BUTTON_SIZE = 10;
// A half of size
const HALF_BUTTON_SIZE = BUTTON_SIZE / 2;
// Default centerdinate
const DEFAULT_ROOT = { x: 50, y: 20 };
// Default height and width of Resizer
const DEFAULT_SIZE = { width: 100, height: 40 };

const StyledText = styled.Text`
background-color: white;
border: 1px dashed green;
`;

let _panResponderBottomRight;
const fnFalse = () => false;
const fnTrue = () => true;

const computeStyled = (center, size) => {
    const { x, y } = center;
    const { width, height } = size;
    return {
        StyledView: styled.View`
        position: absolute;
        background-color: white;
        border: 1px dotted #37516d;
        background-color: transparent;
        height: ${height || DEFAULT_SIZE.height};
        width: ${width || DEFAULT_SIZE.height};
        top: ${y - (height / 2) || DEFAULT_ROOT.y};
        left: ${x - (width / 2) || DEFAULT_ROOT.x};
        `,
        Button: styled.TouchableOpacity`
        width: ${BUTTON_SIZE}px;
        height: ${BUTTON_SIZE}px;
        background-color: #00b6ff;
        border-radius: 20;
        position: absolute;
        top: ${center.y - HALF_BUTTON_SIZE};
        left: ${center.x - HALF_BUTTON_SIZE};
        `,
        // Button in Top-Left
        ButtonTL: styled.TouchableOpacity`
        width: ${BUTTON_SIZE}px;
        height: ${BUTTON_SIZE}px;
        background-color: #00b6ff;
        border-radius: 20;
        position: absolute;
        top: ${- HALF_BUTTON_SIZE};
        left: ${- HALF_BUTTON_SIZE};
        `,

        // Button in Top-Middle
        ButtonTM: styled.TouchableOpacity`
        width: ${BUTTON_SIZE}px;
        height: ${BUTTON_SIZE}px;
        background-color: #00b6ff;
        border-radius: 20;
        position: absolute;
        top: ${- HALF_BUTTON_SIZE};
        left: ${(width / 2) - HALF_BUTTON_SIZE};
        `,

        // Button in Top-Right
        ButtonTR: styled.TouchableOpacity`
        width: ${BUTTON_SIZE}px;
        height: ${BUTTON_SIZE}px;
        background-color: #00b6ff;
        border-radius: 20;
        position: absolute;
        top: ${- HALF_BUTTON_SIZE};
        left: ${width - HALF_BUTTON_SIZE};
        `,
        // Button in Middle-Left
        ButtonML: styled.TouchableOpacity`
        width: ${BUTTON_SIZE}px;
        height: ${BUTTON_SIZE}px;
        background-color: #00b6ff;
        border-radius: 20;
        position: absolute;
        top: ${(height / 2) - HALF_BUTTON_SIZE};
        left: ${- HALF_BUTTON_SIZE};
        `,
        // Button in Center
        ButtonMM: styled.TouchableOpacity`
        width: ${BUTTON_SIZE}px;
        height: ${BUTTON_SIZE}px;
        background-color: #00b6ff;
        border-radius: 20;
        position: absolute;
        top: ${(height / 2) - HALF_BUTTON_SIZE};
        left: ${(width / 2) - HALF_BUTTON_SIZE};
        `,
        // Button in Middle-Right
        ButtonMR: styled.TouchableOpacity`
        width: ${BUTTON_SIZE}px;
        height: ${BUTTON_SIZE}px;
        background-color: #00b6ff;
        border-radius: 20;
        position: absolute;
        top: ${(height / 2) - HALF_BUTTON_SIZE};
        left: ${width - HALF_BUTTON_SIZE};
        `,
        // Button in Bottom-Left
        ButtonBL: styled.TouchableOpacity`
        width: ${BUTTON_SIZE}px;
        height: ${BUTTON_SIZE}px;
        background-color: #00b6ff;
        border-radius: 20;
        position: absolute;
        top: ${height - HALF_BUTTON_SIZE};
        left: ${- HALF_BUTTON_SIZE};
        `,
        // Button in Bottom-Middle
        ButtonBM: styled.TouchableOpacity`
        width: ${BUTTON_SIZE}px;
        height: ${BUTTON_SIZE}px;
        background-color: #00b6ff;
        border-radius: 20;
        position: absolute;
        top: ${height - HALF_BUTTON_SIZE};
        left: ${(width / 2) - HALF_BUTTON_SIZE};
        `,
        // Button in Bottom-Right
        ButtonBR: styled.TouchableOpacity`
        width: ${BUTTON_SIZE}px;
        height: ${BUTTON_SIZE}px;
        background-color: #00b6ff;
        border-radius: 20;
        position: absolute;
        top: ${height - HALF_BUTTON_SIZE};
        left: ${width - HALF_BUTTON_SIZE};
        `,
    }
}

export default class Resizer extends React.Component {
    constructor(props) {
        super(props);
        this.onPressIn = this.onPressIn.bind(this);
        this.onPressIn = this.onPressIn.bind(this);
       
    }
    componentWillMount() {
        
        this.setState({
            center: { x: 50, y: 20 },
            size: { width: 100, height: 40 },
            selectedButton: 'BL',
        });
    }

    onPressIn(e, key) {
        this.setState({
            selectedButton: key
        });
        console.log('end set button:', key);
        
        this.props.canMove && this.props.canMove();
    }

    onResponderMove(coor) {
        console.log('received from parent', coor);
        const { selectedButton, center, size } = this.state;

        // Calculate denta width and height
        const dentaY = (Math.abs(coor.y - center.y) - (size.height / 2));
        const dentaX = (Math.abs(coor.x - center.x) - (size.width / 2));

        // Calculate new center coordinate
        const x = center.x + dentaX / 2;
        const y = center.y + dentaY / 2;

        // Calculate new height and width
        const height = size.height + dentaY;
        const width = size.width + dentaX;

        switch (selectedButton) {
            case 'MM':
                {
                    this.setState({
                        center: coor,
                    })
                }
                break;
            case 'MR':
                {
                    this.setState({
                        center: { x, y: center.y },
                        size: { width, height: size.height },
                    });
                }
                break;
            case 'BM':
                {
                    this.setState({
                        center: { x: center.x, y },
                        size: { width: size.width, height },
                    });
                }
                break;
            case 'BR':
                {
                    this.setState({
                        center: { x, y },
                        size: { width, height },
                    });
                }
                break;
            case 'ML':
                {
                    const x = center.x - dentaX / 2;
                    this.setState({
                        center: { x, y: center.y },
                        size: { width, height: size.height },
                    });
                }
                break;
            case 'BL':
                {
                    const x = center.x - dentaX / 2;
                    this.setState({
                        center: { x, y },
                        size: { width, height },
                    });
                }
                break;
            case 'TR':
                {
                    const y = center.y - dentaY / 2;
                    this.setState({
                        center: { x, y },
                        size: { width, height },
                    });
                }
                break;
            case 'TM':
                {
                    const y = center.y - dentaY / 2;
                    this.setState({
                        center: { x: center.x, y },
                        size: { width: size.width, height },
                    });
                }
                break;
            case 'TL':
                {
                    const x = center.x - dentaX / 2;
                    const y = center.y - dentaY / 2;
                    this.setState({
                        center: { x, y },
                        size: { width, height },
                    });
                }
                break;
            default:
                break;
        }
    }
   
    
    sayHello = () => {
        console.log('called hello from child');
    }
    render() {
        const { center, size } = this.state;
        const {
            StyledView,
            ButtonTL,
            ButtonTM,
            ButtonTR,
            ButtonML,
            ButtonMM,
            ButtonMR,
            ButtonBL,
            ButtonBM,
            ButtonBR } = computeStyled(center, size);
        return (
            <View style={styles.container}>
                <StyledView>
                    <ButtonTL data-tag='TL' onPressIn={e => { this.onPressIn(e, 'TL') }} title='x'></ButtonTL>
                    <ButtonTM data-tag='TM' onPressIn={e => { this.onPressIn(e, 'TM') }} title='x'></ButtonTM>
                    <ButtonTR data-tag='TR' onPressIn={e => { this.onPressIn(e, 'TR') }} title='x'></ButtonTR>
                    <ButtonML data-tag='ML' onPressIn={e => { this.onPressIn(e, 'ML') }} title='x'></ButtonML>
                    <ButtonMM data-tag='MM' onPressIn={e => { this.onPressIn(e, 'MM') }} title='x'></ButtonMM>
                    <ButtonMR data-tag='MR' onPressIn={e => { this.onPressIn(e, 'MR') }} title='x'></ButtonMR>
                    <ButtonBL data-tag='BL' onPressIn={e => { this.onPressIn(e, 'BL') }} title='x'></ButtonBL>
                    <ButtonBM data-tag='BM' onPressIn={e => { this.onPressIn(e, 'BM') }} title='x'></ButtonBM>
                    <ButtonBR data-tag='BR' onPressIn={e => { this.onPressIn(e, 'BR') }} title='x'></ButtonBR>
                </StyledView>
            </View>
        );
    }
}

Resizer.propTypes = {
    editMode: string,
    x: number,
    y: number,
    height: number,
    width: number,
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
}) 
