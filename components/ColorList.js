import React, { Component, PropTypes } from 'react';
import {
  View, Text, StyleSheet,
  TouchableHighlight, ScrollView, ListView,
  AsyncStorage
} from 'react-native';
import ColorButton from './ColorButton';
import ColorForm from './ColorForm';
export default class ColorList extends Component {
  static navigationOptions = {
    title: 'Available colors'
  }
  constructor() {
    super();

    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    const availableColors = [];
    this.state = {
      availableColors,
      dataSource: this.ds.cloneWithRows(availableColors)
    }
    this.newColor = this.newColor.bind(this);
  }
  saveColors(colors) {
    AsyncStorage.setItem(
      '@ColorListStore:Colors',
      JSON.stringify(colors)
    )
  }
  newColor(color) {
    const availableColors = [
      ... this.state.availableColors,
      color
    ];
    this.setState({
      availableColors,
      dataSource: this.ds.cloneWithRows(availableColors)
    });
    this.saveColors(availableColors);
  }
  componentDidMount() {
    AsyncStorage.getItem('@ColorListStore:Colors', (error, data) => {
      if (error)
        console.error('Error loading colors', error);
      else {
        const availableColors = JSON.parse(data);
        this.setState({
          availableColors,
          dataSource: this.ds.cloneWithRows(availableColors)
        })
      }
    })
  }


  render() {
    const { navigate } = this.props.navigation;
    const { dataSource } = this.state;
    return (
      <ListView dataSource={dataSource} style={[styles.container]}
        renderRow={(color) => (<ColorButton backgroundColor={color} onSelect={()=>navigate('Detail',{color})} />)}
        renderHeader={() => (
          <ColorForm navigation={navigate} onNewColor={this.newColor} />
        )}
      >
      </ListView>
    )
  }
}
ColorList.defaultProps = {
  onColorSelected: f => f
}
ColorList.propTypes = {
  onColorSelected: PropTypes.func.isRequired
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20
  },
  button: {
    borderWidth: 2,
    borderRadius: 10,
    margin: 10,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(255,255,255,0.8)'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  sample: {
    height: 20,
    width: 20,
    borderRadius: 10,
    margin: 5,
    backgroundColor: 'white'
  },
  text: {
    fontSize: 30,
    margin: 5
  },
  header: {
    backgroundColor: 'lightgrey',
    paddingTop: 20,
    padding: 10,
    fontSize: 30,
    textAlign: 'center'
  }
})

