import React from 'React';
import { View, StyleSheet, } from 'react-native';
import Resizer from './Resizer';
import ResizeHelper from './ResizeHelper';
const fnTrue = () => true;
const fnFalse = () => false;

export default class Test extends React.Component {
    componentWillMount() {
        const handleBottomRightMove = ({ x, y }) => {
            //console.log({x,y});
            this.triggerResize();
        }
        this._panResponder = ResizeHelper.createPanResponder({
            onStart: handleBottomRightMove,
            onMove: handleBottomRightMove,
        });
        this.setState({
            start: false,
            move: false,
        });
    }
    triggerResize() {
        this.refs.resizer.sayHello();
    }
    
    fnStart = () => {
        return this.state.start && true;
    }
    fnMove = () => {
        return this.state.move && true;
    }

    onResponderMove = (e) =>{
        const coor = { x: e.nativeEvent.pageX, y: e.nativeEvent.pageY };
        console.log(coor);
        this.refs.resizer.onResponderMove(coor);
    }
    onResponderRelease = (e) => {
        console.log('Released');
        this.setState({
            start: false,
            move: false,
        });
    }
    onResponderStart = (e) => {
        console.log(e.nativeEvent.target);
    }
    canMove = () => {
        console.log('triggered can move');
        this.setState({
            start: true,
            move: true,
        });

    }   
    render() {
        return (
            <View style={styles.container}
                onStartShouldSetResponder={this.fnStart}
                onStartShouldSetResponderCapture={this.fnStart}
                onMoveShouldSetResponder={this.fnMove}
                onResponderMove={this.onResponderMove}
                onResponderRelease={this.onResponderRelease}
                onResponderStart ={this.onResponderStart}
            >
                <Resizer ref="resizer" canMove={this.canMove} />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})