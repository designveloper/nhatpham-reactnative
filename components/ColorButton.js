import React, { Component } from 'react';
import {
  AppRegistry,
  View, Text, StyleSheet,
  TouchableHighlight
} from 'react-native';
const styles = StyleSheet.create({
  button: {
    borderWidth: 2,
    borderRadius: 10,
    margin: 10,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(255,255,255,0.8)'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  sample: {
    height: 20,
    width: 20,
    borderRadius: 10,
    margin: 5,
    backgroundColor: 'white'
  },
  text: {
    fontSize: 30,
    margin: 5
  }
})

const ColorButton = ({ backgroundColor, onSelect }) => {
  return (
    <TouchableHighlight style={styles.button} onPress={() => onSelect(backgroundColor)} underlayColor='orange'>
      <View style={styles.row}>
        <View style={[styles.sample, { backgroundColor: backgroundColor }]} />
        <Text style={styles.text}>{backgroundColor}</Text>
      </View>
    </TouchableHighlight>
  );
}
export default ColorButton;