import {PanResponder} from 'react-native';
const fn = () => true;

const ResizeHelper = {
  /**
     * Simplified pan responder wrapper.
     */
  createPanResponder: ({onStart = fn, onMove = fn, onEnd = fn}) => {
    return PanResponder.create({
      onStartShouldSetPanResponder: fn,
      onStartShouldSetPanResponderCapture: fn,
      onMoveShouldSetPanResponder: fn,
      onMoveShouldSetPanResponderCapture: fn,
      onPanResponderTerminationRequest: fn,
      onPanResponderGrant: (evt, state) => {
        console.log('onPanResponderGrant');
        return onStart(
          {x: evt.nativeEvent.pageX, y: evt.nativeEvent.pageY},
          evt,
          state,
        );
      },
      onPanResponderMove: (evt, state) => {
        console.log('onPanResponderMove');
        return onMove(
          {x: evt.nativeEvent.pageX, y: evt.nativeEvent.pageY},
          evt,
          state,
        );
      },
      onPanResponderRelease: (evt, state) => {
        console.log('onPanResponderRelease');
        return onEnd(
          {x: evt.nativeEvent.pageX, y: evt.nativeEvent.pageY},
          evt,
          state,
        );
      },
    });
  },

  /**
     * Rotates point around given center in 2d.
     * Point is object literal { x: number, y: number }
     * @param {point} point to be rotated
     * @param {number} angle in radians
     * @param {point} center to be rotated around
     * @return {point} rotated point
     */
  rotatePoint: (point, angle, center = {x: 0, y: 0}) => {
    // translation to origin
    const transOriginX = point.x - center.x;
    const transOriginY = point.y - center.y;

    // rotation around origin
    const rotatedX =
      transOriginX * Math.cos(angle) - transOriginY * Math.sin(angle);
    const rotatedY =
      transOriginY * Math.cos(angle) + transOriginX * Math.sin(angle);

    // translate back from origin
    const normalizedX = rotatedX + center.x;
    const normalizedY = rotatedY + center.y;
    return {
      x: normalizedX,
      y: normalizedY,
    };
  },
};

export default ResizeHelper;
