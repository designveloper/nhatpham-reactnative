import React, { Component } from 'react';
import {
  AppRegistry, Alert
} from 'react-native';
import ColorList from './components/ColorList';
import ColorDetail from './components/ColorDetail';
// import Resizer from './components/Resizer';
import Test from './components/Test';
import { StackNavigator } from 'react-navigation';
import WebPage from './components/WebPage';

// const App = StackNavigator({
//   Home: { screen: Resizer },
//   Detail: { screen: ColorDetail },
//   Web: { screen: WebPage },
// })

AppRegistry.registerComponent('mnReactNative', () => Test);
